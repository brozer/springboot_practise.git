package trade.axht.springboot.demo.exception.entity;

public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
